const routesList = {
    root: '/',
    profile: '/profile',
    login: '/login',
    start: '/start',
    fns: {
        
    }
}

const redirectUrlKey = 'redirectUrl'

export {
    routesList,
    redirectUrlKey
}