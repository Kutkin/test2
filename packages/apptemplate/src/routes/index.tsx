import { jsx } from '@emotion/react'
import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'

import { routesList } from './routes-list'
import { RequireAuth, OnlyAnonymous } from 'routes/require-auth'
import { AppLayout } from 'kit/layouts'
import { LoginScreen } from 'screens/login'
import { Screen404 } from 'screens/404'
import { ProfileScreen } from 'screens/profile'

function Start() {
    return <p>start</p>
}

export function AppRoutes() {

    return (
        <Routes>
            <Route
                path={routesList.login}
                element={(
                    <OnlyAnonymous>
                        <AppLayout>
                            <LoginScreen />
                        </AppLayout>
                    </OnlyAnonymous>
                )}
            />

            <Route
                path={routesList.start}
                element={(
                    <RequireAuth>
                        <AppLayout>
                            <Start />
                        </AppLayout>
                    </RequireAuth>
                )}
            />

            <Route
                path={routesList.profile}
                element={(
                    <RequireAuth>
                        <AppLayout>
                            <ProfileScreen />
                        </AppLayout>
                    </RequireAuth>
                )
                }
            />

            {/* REDIRECT */}
            <Route
                path="/"
                element={<Navigate to={routesList.start} />}
            />

            <Route
                path="*"
                element={(
                    <AppLayout>
                        <Screen404 />
                    </AppLayout>
                )}
            />
        </Routes >
    )
}