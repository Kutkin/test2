import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { QueryClientProvider } from 'react-query'
import { ThemeProvider } from '@emotion/react'
import { QueryParamProvider } from 'use-query-params'
import { IntlProvider } from 'react-intl'

import { appTheme } from '@workspace/components'
import { GlobalStyles } from '@workspace/components'
import { MediaProvider } from '@workspace/components'

import { AuthProvider } from 'context/auth-context'
import { RouteAdapter } from 'utils/route-adapter'
import { queryClient } from 'utils/react-query-config'
import ru from 'localization/languages/ru.json'

//----------------------------------

function AppProviders({ children }: { children: React.ReactChild }) {
    return (
        <MediaProvider>
            <ThemeProvider theme={appTheme}>
                <GlobalStyles />
                
                <QueryClientProvider client={queryClient}>
                    <Router>
                        <QueryParamProvider ReactRouterRoute={RouteAdapter as any}>
                            <AuthProvider>
                                <IntlProvider
                                    messages={ru}
                                    locale={'ru-RU'}
                                    key={'ru-RU'} // https://formatjs.io/docs/react-intl/components#dynamic-language-selection
                                >
                                    {children}
                                </IntlProvider >
                            </AuthProvider>
                        </QueryParamProvider>
                    </Router>
                </QueryClientProvider>
                
            </ThemeProvider>
        </MediaProvider>
    )
}

export {
    AppProviders
}