import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useIntl } from 'react-intl'

import { useAuth } from 'context/auth-context'

type FormInputs = {
    login: string
    password: string
    common: string
}

const schema = yup.object().shape({
    login: yup.string().required(),
    password: yup.string().required(),
    common: yup.string()
})

function useModule() {
    const { formatMessage } = useIntl()
    const { login, loginIsLoading } = useAuth()

    const formProps = useForm<FormInputs>({
        mode: 'onChange',
        resolver: yupResolver(schema)
    });
    const onSubmit = async (data: FormInputs) => {
        try {
            await login(data)
        } catch (e) {
            const message = (e as any)?.status === 400
                ? formatMessage({ id: 'app.invalidLoginOrPassword' })
                : formatMessage({ id: 'app.loginFailed' })

            formProps.setError("common", { message })
        }
    }

    return {
        ...formProps,

        onSubmit,

        loginIsLoading,
    }
}

export {
    useModule
}