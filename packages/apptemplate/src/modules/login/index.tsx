import { Theme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { useLocation, useNavigate } from 'react-router-dom'

import { routesList } from 'routes/routes-list'
import { Card, CardContent, H4, P, Button, Input } from '@workspace/components'
import { useModule } from './dependencies/use-module'

const Frame = styled.div(({ theme }) => {
    return {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

const Form = styled.form(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    }
})

const Error = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        marginTop: 24
    }
})

function Login() {
    const navigate = useNavigate()

    const {
        loginIsLoading,

        register,
        handleSubmit,
        formState: {
            errors,
            isValid,
            isSubmitting
        },

        onSubmit,
    } = useModule()

    const { formatMessage } = useIntl()

    return (
        <Frame>
            <Card style={{ maxWidth: 370, width: 'calc(100% - 48px)', marginBottom: 12, marginTop: 12 }}>
                <CardContent style={{
                    paddingTop: 24,
                    paddingBottom: 24,
                    alignItems: 'center'
                }}>
                    <H4 style={{ textAlign: 'center' }}>
                        <FormattedMessage id='app.welcome' />
                    </H4>
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            style={{ width: '100%' }}
                            intent={errors.common?.message ? 'danger' : 'none'}
                            placeholder={formatMessage({ id: 'app.login' })}
                            {...register('login')}
                        />
                        <Input
                            style={{ width: '100%' }}
                            intent={errors.common?.message ? 'danger' : 'none'}
                            placeholder={formatMessage({ id: 'app.password' })}
                            type='password'
                            {...register('password')}
                        />
                        <Button
                            type='submit'
                            style={{ width: '100%' }}
                            loading={loginIsLoading}
                        >
                            <FormattedMessage id='app.loginAction' />
                        </Button>
                        {errors.common?.message && (
                            <Error>
                                <P
                                    intent={'danger'}
                                    style={{ textAlign: 'center' }}
                                >
                                    {errors.common?.message}
                                </P>
                            </Error>
                        )}
                    </Form>
                </CardContent>
            </Card>
        </Frame>
    )
}

export {
    Login
}