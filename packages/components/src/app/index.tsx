import { jsx, Theme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'

import { Frame } from './dependencies/frame'

import { Example as CardExample } from '../components/card/example'
import { Example as ButtonExample } from '../components/button/example'
import { Example as SimpleTableExample } from '../components/simple-table/example'
import { Example as CalloutExample } from '../components/callout/example'
import { Example as LiveArrowExample } from '../components/live-arrow/example'
import { Example as CheckboxExample } from '../components/checkbox/example'
import { Example as InputExample } from '../components/input/example'
import { Example as InputGroupExample } from '../components/input-group/example'
import { Example as ErrorExample } from '../components/error/example'
import { Example as InputMaskExample } from '../components/input-mask/example'
import { Example as ButtonMinimalExample } from '../components/button-minimal/example'
import { Example as SecondsTimerExample } from '../components/seconds-timer/example'

const MainFrame = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        alignItems: 'center',
        marginBottom: 24
    }
})

export function App() {

    React.useEffect(() => {
        const controlToScrollTo = document.getElementById(window.location.hash.replace('#', ''))
        controlToScrollTo?.scrollIntoView()
    }, [])

    return (
        <MainFrame>
            <Frame title='SecondsTimer'>
                <SecondsTimerExample />
            </Frame>

            <Frame title='ButtonMinimal'>
                <ButtonMinimalExample />
            </Frame>

            <Frame title='Card'>
                <CardExample />
            </Frame>

            <Frame title='Button'>
                <ButtonExample />
            </Frame>

            <Frame title='SimpleTable'>
                <SimpleTableExample />
            </Frame>

            <Frame title='Callout'>
                <CalloutExample />
            </Frame>

            <Frame title='LiveArrow'>
                <LiveArrowExample />
            </Frame>

            <Frame title='Checkbox'>
                <CheckboxExample />
            </Frame>

            <Frame title='Input'>
                <InputExample />
            </Frame>

            <Frame title='InputGroup'>
                <InputGroupExample />
            </Frame>

            <Frame title='Error'>
                <ErrorExample />
            </Frame>

            <Frame title='InputMask'>
                <InputMaskExample />
            </Frame>
        </MainFrame>
    )
}