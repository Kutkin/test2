import { jsx } from '@emotion/react'
import React from 'react'

import { Input } from './index'

function Example() {
    return (
        <div>
            <Input
                value='Мирослав'
                onChange={e => {

                }}
            />
            <Input
                intent='danger'
                onChange={e => {
                    
                }}
            />
        </div>
    )
}

export {
    Example
}