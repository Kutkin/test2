import { jsx, Theme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'

import { Footer } from './dependencies/footer'

type DefaultLayoutProps = {
	children: React.ReactNode,
	header: React.ReactNode
}

const Frame = styled.div(({ theme }: {
	theme?: Theme
}) => {
	return {
		display: 'flex',
		overflow: 'hidden',
		flexDirection: 'column',
		minHeight: '100vh',
		overflowY: 'auto'
	}
})

const Body = styled.div(({ theme }: {
	theme?: Theme
}) => {
	return {
		display: 'flex',
		flexDirection: 'column',
		width: '100%',
		flexGrow: 1,
		background: theme!.colors.color8
	}
})

function DefaultLayout({
	children,
	header
}: DefaultLayoutProps) {

	return (
		<Frame>
			{header}
			<Body>
				{children}
			</Body>
			<Footer />
		</Frame>
	)
}

export {
	DefaultLayout
}