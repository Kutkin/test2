import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider } from '@emotion/react'

import { App } from './app'

import { appTheme } from './theme'
import { MediaProvider } from './theme/media-provider'
import { GlobalStyles } from './theme/global-styles'
import * as utils from './utils'
import * as validation from '../src/validation/personal-data'

ReactDOM.render(
  <React.StrictMode>
    <MediaProvider>
      <ThemeProvider theme={appTheme}>
        <GlobalStyles />
        <App />
      </ThemeProvider>
    </MediaProvider>
  </React.StrictMode>,
  document.getElementById('root')
)

export * from './theme'
export * from './theme/global-styles'
export * from './theme/media-provider'

export * from '../src/components/card'
export * from '../src/components/button'
export * from '../src/components/simple-table'
export * from '../src/components/callout'
export * from '../src/components/live-arrow'
export * from '../src/components/checkbox'
export * from '../src/components/input'
export * from '../src/components/input-group'
export * from '../src/components/error'
export * from '../src/components/input-mask'
export * from '../src/components/button-minimal'
export * from '../src/components/seconds-timer'
export * from '../src/components/fancy-link'
export * from '../src/components/typography'
export * from '../src/components/dropzone'
export * from '../src/components/notify'
export * from '../src/components/spinner'
export * from '../src/components/nav-link'
export * from '../src/components/full-page-loader'
export * from '../src/components/animations/svg-rotator'
export * from '../src/components/layouts'


export {
  utils,
  validation
}