import { Theme, useTheme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link as reactRouterLink } from 'react-router-dom'

import { routesList } from 'routes/routes-list'
import { ReactComponent as DesktopLogo } from 'assets/images/desktop_logo.svg'
import { ReactComponent as MobileLogo } from 'assets/images/mobile_logo.svg'
import { ReactComponent as LogoutIcon } from 'assets/images/logout.svg'
import { useMedia } from '@workspace/components'
import { useAuth } from 'context/auth-context'
import { ButtonMinimal, Link, NavLink } from '@workspace/components'

const LogoLink = styled(reactRouterLink)(({ theme }: {
    theme?: Theme
}) => {
    return {
        textDecoration: 'none',
        margin: 0,
        padding: 0,
        ':hover': {
            cursor: 'pointer'
        }
    }
})

const Frame = styled.header(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        flexShrink: 0,
        background: theme!.colors.color4,
        borderBottom: `1px solid ${theme!.colors.color2}`,

        [theme!.mq.small]: {
            height: 64,
            display: 'flex',
            alignItems: 'center'
        }
    }
})

const Alignment = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        maxWidth: 1168,
        padding: '8px 0',

        [theme!.mq.small]: {
            height: 64,
            padding: '0 24px',
        }
    }
})

const Info = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        color: theme!.colors.color1,
        fontSize: 10,
        lineHeight: '14px'
    }
})

const DesktopLi = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        fontSize: 18,
        margin: '0 16px',
        lineHeight: '24px',
        color: theme!.colors.color5,
        display: 'flex',
        alignItems: 'center'
    }
})

const LogOutButtonWrapper = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        [theme!.mq.small]: {
            display: 'flex',
            justifyContent: 'flex-end',
            width: 230
        }
    }
})

function Header() {
    const { user, logout } = useAuth()
    const { isMobile } = useMedia()
    const { colors } = useTheme()

    return (
        <Frame>
            <Alignment>
                <LogoLink to={routesList.root}>
                    {isMobile ? <MobileLogo style={{ paddingLeft: 24 }} /> : <DesktopLogo />}
                </LogoLink>
                {!user && (
                    <Info>
                        <Link style={{
                            fontSize: isMobile ? 13 : 17,
                            fontWeight: 600,
                            lineHeight: '150%',
                            textDecoration: 'none',
                            color: colors.color5,
                            margin: 0
                        }} href="tel:88001007269">8 800 100 72 69</Link>
                        <span><FormattedMessage id='app.callFree' /></span>
                    </Info>
                )}
                {user && (
                    <React.Fragment>
                        {!isMobile && (
                            <div style={{
                                display: 'flex',
                                height: 64,
                            }}>
                                <DesktopLi>
                                    <NavLink
                                        to="/orders"
                                        type='desktop-menu'
                                        style={({ isActive }) => ({
                                            fontWeight: isActive ? 600 : 'inherit',
                                            borderBottomWidth: isActive ? 1 : 0
                                        })}
                                    >
                                        <FormattedMessage id='app.menu.shipments' />
                                    </NavLink>
                                </DesktopLi>
                                <DesktopLi>
                                    <NavLink
                                        to="/profile"
                                        type='desktop-menu'
                                        style={({ isActive }) => ({
                                            fontWeight: isActive ? 600 : 'inherit',
                                            borderBottomWidth: isActive ? 1 : 0
                                        })}
                                    >
                                        <FormattedMessage id='app.menu.profile' />
                                    </NavLink>
                                </DesktopLi>
                            </div>
                        )}
                        <LogOutButtonWrapper>
                            <ButtonMinimal
                                icon={<LogoutIcon style={{ width: 20, height: 20, marginRight: 4 }} />}
                                onClick={() => logout()}
                            >
                                <FormattedMessage id='app.logout' />
                            </ButtonMinimal>
                        </LogOutButtonWrapper>
                    </React.Fragment>
                )}
            </Alignment>
            {isMobile && (
                <div style={{
                    display: 'flex',
                    padding: 12,
                    justifyContent: 'space-around',
                    borderTop: `1px solid ${colors.color2}`
                }}>
                    <NavLink
                        to="/orders"
                        type='mobile-menu'
                        style={({ isActive }) => ({
                            color: isActive ? colors.color3 : 'inherit',
                            fontWeight: isActive ? 600 : 'inherit'
                        })}
                    >
                        <FormattedMessage id='app.menu.shipments' />
                    </NavLink>
                    <NavLink
                        to="/profile"
                        type='mobile-menu'
                        style={({ isActive }) => ({
                            color: isActive ? colors.color3 : 'inherit',
                            fontWeight: isActive ? 600 : 'inherit'
                        })}
                    >
                        <FormattedMessage id='app.menu.profile' />
                    </NavLink>
                </div>
            )}
        </Frame >
    )
}

export {
    Header
}