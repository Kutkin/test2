import { jsx, Theme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'

import { DefaultLayout } from '@workspace/components'

import { Header } from './dependencies/header'

type AppLayoutProps = {
    children: React.ReactNode
}

const Frame = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {

    }
})

function AppLayout(props: AppLayoutProps) {

    return (
        <DefaultLayout
            header={<Header />}
            {...props}
        />
    )
}

export {
    AppLayout
}