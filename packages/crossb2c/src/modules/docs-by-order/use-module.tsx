import React from 'react'
import { useForm, useWatch } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { yupResolver } from '@hookform/resolvers/yup'
import { useIntl } from 'react-intl'
import { useMutation, useQuery } from 'react-query'
import { parse, format, parseISO } from 'date-fns'
import { AxiosResponse } from 'axios'
import debounce from 'debounce-promise'

import { notify } from '@workspace/components'
import { routesList } from 'routes/routes-list'
import { useClient } from 'context/auth-context'
import { GetPersonalInfoV2_ServerResponse } from 'api/typings/get-personal-info-v2'
import { PutPersonalInfoV2_ServerRequest } from 'api/typings/put-personal-info-v2'
import { GetPersonalBySberId_ServerResponse } from 'api/typings/get-personal-by-sber-id'
import { toBase64, capitalizeFirstLetter } from 'utils'

import { validation } from '@workspace/components'

import { UserFormModel } from './types'

const dateFormat = 'dd.MM.yyyy'

function convertToISOString(date?: string): string | undefined {
    if (!date) {
        return date
    }

    const target = parse(date!, dateFormat, new Date())

    return `${target.getFullYear()}-${(target.getMonth() + 1).toString().padStart(2, '0')}-${target.getDate().toString().padStart(2, '0')}T00:00:00.000Z`
}

function toMaskDate(date?: string): string | undefined {
    return date ? format(parseISO(date), dateFormat) : date
}

function enterTransform(initialModel: GetPersonalInfoV2_ServerResponse): UserFormModel {
    return {
        ...initialModel,

        docType: initialModel.docType ?? validation.personalData.passportTypeEnum.rf,
        birthDate: toMaskDate(initialModel.birthDate),
        docDate: toMaskDate(initialModel.docDate),

        userDocs: []
    }
}

async function exitTransform(exitModel: UserFormModel): Promise<PutPersonalInfoV2_ServerRequest> {
    const { metadata, userDocs, ...others } = exitModel

    const result: PutPersonalInfoV2_ServerRequest = {
        ...others,

        patronymic: exitModel.patronymic ? exitModel.patronymic : null,
        taxNumber: exitModel.taxNumber ? exitModel.taxNumber : null,
        birthDate: convertToISOString(exitModel.birthDate),
        docDate: convertToISOString(exitModel.docDate),
    }

    if (userDocs.length === 1) {
        const doc = userDocs[0]

        let base64content = await toBase64(doc)

        if (typeof base64content === 'string') {
            result.document = {
                docName: doc.name,
                docBody: base64content.split('base64,')[1],
                contentType: doc.type
            }

            return result
        }
    }

    return result
}

function useModule(initialModel: GetPersonalInfoV2_ServerResponse) {
    const { formatMessage } = useIntl()
    const client = useClient()
    const navigate = useNavigate()

    const { current: checkPassportDebounced } = React.useRef(debounce(x => client.checkPassport(x), 150))

    const { current: schema } = React.useRef(validation.personalData.createPersonalDataValidationSchema({
        formatMessage,
        checkPassportDebounced,
        isDocsRequired: initialModel.metadata.isDocsRequired
    }))

    const [isAgreementAccept, setIsAgreementAccept] = React.useState(false)

    const formProps = useForm<UserFormModel>({
        mode: 'onChange',
        resolver: yupResolver(schema),
        defaultValues: enterTransform(initialModel)
    })

    const updateQuery = useMutation(async () => {
        let correctValues: PutPersonalInfoV2_ServerRequest

        try {
            correctValues = await exitTransform(formProps.getValues()) // если брать из Объекта data, то даты (в смысле дни) почему-то приведены к объекту Date!
        } catch {
            notify({
                message: 'Не удалось преобразовать файл',//formatMessage({ id: 'app.docs.failFileConvert' }),
                type: 'error'
            })

            throw new Error()
        }

        return client.saveProfile(correctValues)
    }, {
        onSuccess: response => {
            formProps.reset()

            notify({
                message: formatMessage({ id: 'app.docs.formUpdateSuccess' }),
                type: 'success'
            })

            setTimeout(() => {
                navigate(routesList.orders)
            }, 100)
        },
        onError: (e: HttpErrorResponse<keyof GetPersonalInfoV2_ServerResponse>) => {
            try {
                if (e.response?.data?.title) {
                    notify({
                        message: e.response?.data?.title,
                        type: 'error'
                    })
                }

                e.response?.data.errors?.forEach(x => {
                    formProps.setError(x.fieldName!, {
                        type: 'required',
                        message: x.message?.join(', ')
                    }, { shouldFocus: true })
                })
            } catch (e) {
                notify({
                    message: formatMessage({ id: 'app.docs.formUpdateError' }),
                    type: 'error'
                })
            }
        }
    })

    const taxNumberQuery = useMutation(() => {
        const values = formProps.getValues()
        return client.getTaxNumber({
            birthDate: convertToISOString(values.birthDate)!,
            docType: values.docType!,
            lastName: values.lastName?.trim()!,
            name: values.name?.trim()!,
            passportNumber: values.docSeries! + values.docNumber!,

            docDate: convertToISOString(values.docDate), // optional
            middleName: values.patronymic?.trim(), // optional
        })
    }, {
        onSuccess: response => {
            formProps.setValue('taxNumber', response.data.taxNumber, {
                shouldValidate: true,
                shouldDirty: true,
                shouldTouch: true
            })
            notify({
                message: formatMessage({ id: 'app.docs.getInnSuccess' }, { value: response.data.taxNumber }),
                type: 'success'
            })
        },
        onError: (e: HttpErrorResponse) => {
            notify({
                message: e?.response?.data?.title || formatMessage({ id: 'app.docs.getInnError' }),
                type: 'error'
            })
        }
    })

    const sberIdDataQuery = useQuery<AxiosResponse<GetPersonalBySberId_ServerResponse>, HttpErrorResponse>(client.getPersonalInfoBySberId.toString(), () => {
        return client.getPersonalInfoBySberId()
    }, {
        enabled: false,
        onSuccess: response => {
            const formFields =
                enterTransform({
                    ...response.data, ...{
                        metadata: { //фейковое расширение сттруктуры для совместимости с типом
                            isDocsRequired: true,
                            isPaymentRequired: true,
                            isPersonalDataFromSberIdAvailable: true
                        }
                    }
                })

            function normalizeSberIdValues(field: keyof typeof formFields, value: string) {
                switch (field) {
                    case 'email': return value.toLowerCase()
                    case 'docType': return value
                    default: return capitalizeFirstLetter(value)
                }
            }

            Object
                .entries(formFields)
                .filter(x => typeof x[1] == 'string')
                .forEach(x => {
                    const field = x[0] as keyof typeof formFields
                    formProps.setValue(
                        field,
                        x[1],
                        {
                            shouldDirty: true,
                            shouldTouch: true,
                            shouldValidate: false
                        }
                    )
                })

            formProps.trigger()

            notify({
                message: formatMessage({ id: 'app.docs.formSbedIdUpdateSuccess' }),
                type: 'success'
            })
        },
        onError: response => {
            notify({
                message: formatMessage({ id: 'app.docs.formSbedIdUpdateError' }),
                type: 'error'
            })
        }
    })

    const trigger = formProps.trigger
    const birthDate = useWatch({ control: formProps.control, name: 'birthDate' })
    React.useEffect(() => {
        trigger('docDate')
    }, [birthDate, trigger])

    const docSeries = useWatch({ control: formProps.control, name: 'docSeries' })
    React.useEffect(() => {
        trigger('docNumber')
    }, [docSeries, trigger])

    const onSubmit = async () => {
        try {
            updateQuery.mutateAsync()
        } catch (e) {

        }
    }

    const isTaxNumberAvailable = (
        !formProps.formState.errors.birthDate &&
        formProps.getValues('docType') === validation.personalData.passportTypeEnum.rf &&
        !formProps.formState.errors.lastName &&
        !formProps.formState.errors.name &&
        !formProps.formState.errors.docSeries &&
        !formProps.formState.errors.docNumber
    )

    return {
        formProps,

        onSubmit,

        isAgreementAccept,
        toggleIsAgreementAccept: setIsAgreementAccept,

        taxNumberQuery,

        isTaxNumberAvailable,

        sberIdDataQuery,
    }
}

export {
    useModule
}
