function refreshToken() {
    return _client('api/v1/auth/refresh-token', null, null, 'GET')
}

function login({ login, password }) {
    return _client('api/v1/auth/signin', null, { login, password }, 'POST')
}

async function logout() {
    try {
        const response = await refreshToken()
        return _client('api/v1/auth/logout', response.accessToken, null, 'POST')
    } catch (e) {
        return Promise.resolve({ status: 200 })
    }
}

async function _client(endpoint, token, data, method) {
    const config = {
        method,
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include'
    }

    if (data) {
        config.body = JSON.stringify(data)
    }

    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
    }

    return window.fetch(`${window._env_.REACT_APP_AUTH_URL}/${endpoint}`, config).then(async response => {
        if (response.ok) {
            try {
                return await response.json()
            } catch (e) {
                return response
            }
        } else {
            return Promise.reject(response)
        }
    })
}


export {
    refreshToken,

    login,
    logout
}