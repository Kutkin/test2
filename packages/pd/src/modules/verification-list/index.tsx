import { jsx, Theme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { routesList } from 'routes/routes-list'

import {
    Input,
    Button,
    H1
} from '@workspace/components'

const Frame = styled.form(({ theme }: {
    theme?: Theme
}) => {
    return {
        marginTop: '60px',
        width: '100%',

    }
})

function VerificationList() {
    const navigate = useNavigate()
    const [phone, setPhone] = React.useState('')

    return (
        <Frame
            onSubmit={() => {
                navigate(routesList.fns.createVerificationByPhoneLink(phone))
            }}
        >
            <H1 style={{ textAlign: 'center' }}>
                Поиск ПД по номеру телефона
            </H1>

            <div
                style={{
                    justifyContent: 'center',
                    display: 'flex',
                    gap: 8
                }}
            >
                <Input
                    placeholder='Введите номер телефона'
                    type={'number'}
                    value={phone}
                    onChange={e => {
                        setPhone(e.target.value)
                    }}
                />

                <Button
                    disabled={phone.length < 10}
                    type='submit'
                >
                    Перейти
                </Button>
            </div>
        </Frame>
    )
}

export {
    VerificationList
}