import { Theme, useTheme } from '@emotion/react'
import styled from '@emotion/styled'
import { FormattedMessage } from 'react-intl'
import { Controller } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
    FancyLink,
    Card,
    CardContent,
    Input,
    Button,
    InputGroup,
    BottomHint,
    H3,
    InputMask,
    MASK,
    ButtonMinimal,
    Spinner,
    useMedia,
    validation,
    utils
} from '@workspace/components'

import { routesList } from 'routes/routes-list'
import { GetPersonalInfo_ServerResponse } from 'api/typings/get-personal-info'
import { ReactComponent as SearchIcon } from 'assets/images/search.svg'
import { ReactComponent as ArrowLeftIcon } from 'assets/images/arrow_left.svg'

import { useModule } from './use-module'

const {
    personalData: {
        passportTypeEnum
    }
} = validation

type PdVerificationProps = {
    initialModel: GetPersonalInfo_ServerResponse
}

const Frame = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%'
    }
})

const CenteredBlock = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: 'calc(100% - 48px)',
        maxWidth: 770,
    }
})

const Form = styled.form(({ theme }) => {
    return {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    }
})

const InputRow = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        marginBottom: 30,
        width: '100%',
        display: 'flex',
        flexDirection: 'column',

        [theme!.mq.small]: {
            marginBottom: 24,
            alignItems: 'center',
            flexDirection: 'row',
            '> button': {
                marginBottom: 0
            },
        }
    }
})

const InputTitle = styled.p(({ theme }: {
    theme?: Theme
}) => {
    return {
        fontWeight: 600,
        fontSize: '15px',
        lineHeight: '135%',
        flexGrow: 1,
        maxWidth: 200,
        color: theme?.colors.color5,
        marginBottom: 8,

        [theme!.mq.small]: {
            marginBottom: 0
        }
    }
})

const Title = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        width: '100%',
        margin: '32px 0 8px 0',
        textAlign: 'center',

        [theme!.mq.small]: {
            margin: '40px 0 12px 0',
            '> a': {
                width: '100%',
                marginBottom: 0,
            },
        }
    }
})

const PassportTypesTab = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        marginRight: 'auto',
        marginBottom: 24,
        '> button': {
            marginBottom: 8
        },

        [theme!.mq.small]: {
            flexDirection: 'row',
            '> button': {
                marginBottom: 0
            },
        }
    }
})

const InnBox = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        width: '100%',
        maxWidth: 520,

        [theme!.mq.small]: {
            maxWidth: 246,
            marginRight: 32
        }
    }
})

const ButtonsBlock = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        width: '100%',
        flexDirection: 'column',
        display: "flex",
        gap: 8,

        [theme!.mq.small]: {
            flexDirection: 'row'
        }
    }
})

const DocumentBlock = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        margin: '0 0 24px 0',
        paddingTop: 8,
        width: '100%',
        display: "flex",
        flexDirection: 'column',
        borderTop: `1px solid ${theme?.colors.color2}`
    }
})

const Document = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        padding: 12
    }
})

function PdVerification({
    initialModel,
}: PdVerificationProps) {
    const {
        formProps: {
            control,
            register,
            trigger,
            watch,
            handleSubmit,
            setValue,
            formState: {
                errors,
                dirtyFields,
                isValid,
                isSubmitSuccessful,
                isDirty,
                isSubmitting
            },
        },
        onSubmit,

        isAgreementAccept,
        toggleIsAgreementAccept,

        taxNumberQuery,

        isTaxNumberAvailable,

        sberIdDataQuery,
        getDocumentQuery,
        confirmQuery,
        rejectQuery
    } = useModule(initialModel)

    const { isMobile } = useMedia()
    const theme = useTheme()
    const navigate = useNavigate()

    const isDisable = true

    return (
        <Frame>
            <CenteredBlock>
                <Title>
                    <FancyLink
                        to={routesList.root}
                    >
                        <ArrowLeftIcon style={{ marginRight: 4 }} /> <FormattedMessage id='app.goBack' />
                    </FancyLink>
                </Title>

                <Card style={{ marginBottom: 48 }}>
                    <CardContent>
                        <Form onSubmit={handleSubmit(onSubmit)}>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.surname' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.lastName?.message}
                                >
                                    <Input
                                        readOnly
                                        style={{ width: '100%' }}
                                        //intent={utils.formSwitchIntent(errors.lastName?.message, dirtyFields.lastName)}
                                        {...register('lastName')}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.name' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.name?.message}
                                >
                                    <Input
                                        readOnly
                                        style={{ width: '100%' }}
                                        //intent={utils.formSwitchIntent(errors.name?.message, dirtyFields.name)}
                                        {...register('name')}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.patronymic' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.patronymic?.message}
                                >
                                    <Input
                                        readOnly
                                        style={{ width: '100%' }}
                                        //intent={utils.formSwitchIntent(errors.patronymic?.message, dirtyFields.patronymic)}
                                        {...register('patronymic')}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.birthDate' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.birthDate?.message}
                                >
                                    <Controller
                                        name='birthDate'
                                        control={control}
                                        render={({ field }) => {
                                            return (
                                                <InputMask
                                                    readOnly
                                                    {...field}
                                                    style={{ width: '100%', marginRight: 16 }}
                                                    mask={MASK.date}
                                                //intent={utils.formSwitchIntent(errors.birthDate?.message, dirtyFields.birthDate)}
                                                />
                                            )
                                        }}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.phone' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                >
                                    <Input
                                        style={{ width: '100%' }}
                                        readOnly
                                        intent={'none'}
                                        {...register('phoneNumber')}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.email' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.email?.message}
                                >
                                    <Input
                                        style={{ width: '100%' }}
                                        //intent={formSwitchIntent(errors.email?.message, dirtyFields.email)}
                                        {...register('email')}
                                        readOnly
                                    />
                                </InputGroup>
                            </InputRow>

                            <PassportTypesTab>
                                <Button
                                    type='button'
                                    // disabled={isDisable}
                                    intent={watch('docType') === passportTypeEnum.rf ? 'primary' : 'none'}
                                    // onClick={() => {
                                    //     setValue('docType', passportTypeEnum.rf, { shouldDirty: true, shouldTouch: true, shouldValidate: true })
                                    //     trigger(['docSeries', 'docNumber', 'taxNumber', 'name', 'patronymic', 'lastName'])
                                    // }}
                                    style={{ marginRight: 6, borderRadius: 8 }}
                                >
                                    <FormattedMessage id='app.docs.rfPassport' />
                                </Button>
                                <Button
                                    type='button'
                                    // disabled={isDisable}
                                    intent={watch('docType') === passportTypeEnum.foreign ? 'primary' : 'none'}
                                    // onClick={() => {
                                    //     setValue('docType', passportTypeEnum.foreign, { shouldDirty: true, shouldTouch: true, shouldValidate: true })
                                    //     trigger(['docSeries', 'docNumber', 'taxNumber', 'name', 'patronymic', 'lastName'])
                                    // }}
                                    style={{ borderRadius: 8 }}
                                >
                                    <FormattedMessage id='app.docs.otherPassport' />
                                </Button>
                            </PassportTypesTab>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.passportSeriesAndNumber' />
                                </InputTitle>
                                <div style={{ position: 'relative', maxWidth: 520, display: 'flex', flexDirection: 'column', flexGrow: 1 }}>
                                    <div style={{ position: 'relative', maxWidth: 520, display: 'flex' }}>
                                        <InputGroup
                                            intent={'danger'}
                                            style={{ width: 100, marginRight: 16 }}
                                        >
                                            <Input
                                                readOnly
                                                style={{ width: '100%', marginRight: 16 }}
                                                //intent={utils.formSwitchIntent(errors.docSeries?.message || errors.docNumber?.message, dirtyFields.docSeries || dirtyFields.docNumber)}
                                                {...register('docSeries')}
                                            />
                                        </InputGroup>

                                        <InputGroup
                                            intent={'danger'}
                                            style={{ width: 120, }}
                                        >
                                            <Input
                                                readOnly
                                                style={{ width: '100%' }}
                                                //intent={utils.formSwitchIntent(errors.docSeries?.message || errors.docNumber?.message, dirtyFields.docSeries || dirtyFields.docNumber)}
                                                {...register('docNumber')}
                                            />
                                        </InputGroup>
                                    </div>
                                    {(errors.docNumber?.message || errors.docSeries?.message) && (
                                        <BottomHint
                                            style={{ marginBottom: 24, color: theme.fns.intentToColor('danger'), top: 52 }}
                                        >
                                            {errors.docSeries?.message || errors.docNumber?.message}
                                        </BottomHint>
                                    )}
                                </div>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.passportDate' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.docDate?.message}
                                >
                                    <Controller
                                        name='docDate'
                                        control={control}
                                        render={({ field }) => {
                                            return (
                                                <InputMask
                                                    readOnly
                                                    {...field}
                                                    style={{ width: '100%', marginRight: 16 }}
                                                    mask={MASK.date}
                                                //intent={utils.formSwitchIntent(errors.docDate?.message, dirtyFields.docDate)}
                                                />
                                            )
                                        }}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.docOrganization' />
                                </InputTitle>
                                <InputGroup
                                    intent={'danger'}
                                    style={{ flexGrow: 1, maxWidth: 520 }}
                                    bottomHint={errors.docOrganization?.message}
                                >
                                    <Input
                                        readOnly
                                        style={{ width: '100%' }}
                                        //intent={utils.formSwitchIntent(errors.docOrganization?.message, dirtyFields.docOrganization)}
                                        {...register('docOrganization')}
                                    />
                                </InputGroup>
                            </InputRow>

                            <InputRow>
                                <InputTitle>
                                    <FormattedMessage id='app.docs.taxNumber' />
                                </InputTitle>
                                <InnBox
                                    style={{ marginBottom: isMobile ? 24 : 0 }}
                                >
                                    <InputGroup
                                        intent={'danger'}
                                        bottomHint={errors.taxNumber?.message}
                                    >
                                        <Controller
                                            name='taxNumber'
                                            control={control}
                                            render={({ field }) => {
                                                return (
                                                    <InputMask
                                                        readOnly
                                                        {...field}
                                                        style={{ width: '100%' }}
                                                        mask={MASK.taxNumber}
                                                    //intent={utils.formSwitchIntent(errors.taxNumber?.message, dirtyFields.taxNumber)}
                                                    />
                                                )
                                            }}
                                        />
                                    </InputGroup>
                                </InnBox>
                                <ButtonMinimal
                                    icon={<SearchIcon />}
                                    onClick={() => {
                                        taxNumberQuery.mutate()
                                    }}
                                    // disabled={!isTaxNumberAvailable || taxNumberQuery.isLoading}
                                    disabled={isDisable}
                                >
                                    <FormattedMessage
                                        id='app.docs.searchTaxNumber'
                                    />
                                </ButtonMinimal>
                                {taxNumberQuery.isLoading && <Spinner size={17} />}
                            </InputRow>

                            <H3 style={{ marginRight: 'auto', width: '100%', marginBottom: 12 }}>
                                <FormattedMessage id='app.docs.docs' />
                            </H3>

                            <DocumentBlock>
                                {initialModel.docs?.map(x => (
                                    <Document key={x.id}>
                                        <div
                                            style={{
                                                display: 'flex',
                                                flexGrow: 1,
                                                paddingRight: 8
                                            }}>
                                            <span
                                                style={{
                                                    cursor: 'pointer',
                                                    color: theme.colors.color3
                                                }}
                                                onClick={() => {
                                                    getDocumentQuery.mutate(x.id)
                                                }}
                                            >
                                                {x.name}
                                            </span>
                                        </div>
                                        <div
                                            style={{
                                                display: 'flex',
                                                flexShrink: 0
                                            }}>
                                            {x.date}
                                        </div>
                                    </Document>
                                ))}
                            </DocumentBlock>

                            <ButtonsBlock>
                                <Button
                                    type='submit'
                                    disabled={confirmQuery.isLoading || rejectQuery.isLoading}
                                    loading={isSubmitting}
                                    style={{ width: '100%', marginBottom: 8 }}
                                    onClick={() => {
                                        confirmQuery.mutate(initialModel.personalData.phoneNumber!)
                                    }}
                                >
                                    <FormattedMessage id='app.confirm' />
                                </Button>
                                <Button
                                    type='button'
                                    intent='warning'
                                    disabled={confirmQuery.isLoading || rejectQuery.isLoading}
                                    loading={isSubmitting}
                                    style={{ width: '100%', marginBottom: 8 }}
                                    onClick={() => {
                                        rejectQuery.mutate(initialModel.personalData.phoneNumber!)
                                    }}
                                >
                                    <FormattedMessage id='app.reject' />
                                </Button>
                                <Button
                                    type='button'
                                    intent='none'
                                    disabled={confirmQuery.isLoading || rejectQuery.isLoading}
                                    style={{ width: '100%', marginBottom: 8 }}
                                    onClick={() => {
                                        navigate(routesList.verificationList)
                                    }}
                                >
                                    <FormattedMessage id='app.cancel' />
                                </Button>
                            </ButtonsBlock>

                            <utils.TriggerFormInitially
                                trigger={trigger}
                            />

                        </Form>
                    </CardContent>
                </Card>
            </CenteredBlock>
        </Frame >
    )
}

export {
    PdVerification
}

export type {
    PdVerificationProps
}
