import React from 'react'
import { DefaultLayout } from '@workspace/components'

import { Header } from './dependencies/header'

type AppLayoutProps = {
    children: React.ReactNode
}

function AppLayout(props: AppLayoutProps) {

    return (
        <DefaultLayout
            header={<Header />}
            {...props}
        />
    )
}

export {
    AppLayout
}