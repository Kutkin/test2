import { Theme, useTheme } from '@emotion/react'
import styled from '@emotion/styled'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link as reactRouterLink } from 'react-router-dom'

import { routesList } from 'routes/routes-list'
import { ReactComponent as DesktopLogo } from 'assets/images/desktop_logo.svg'
import { ReactComponent as MobileLogo } from 'assets/images/mobile_logo.svg'
import { ReactComponent as LogoutIcon } from 'assets/images/logout.svg'
import { useMedia } from '@workspace/components'
import { useAuth } from 'context/auth-context'
import { ButtonMinimal, Link } from '@workspace/components'

const LogoLink = styled(reactRouterLink)(({ theme }: {
    theme?: Theme
}) => {
    return {
        textDecoration: 'none',
        margin: 0,
        padding: 0,
        ':hover': {
            cursor: 'pointer'
        }
    }
})

const Frame = styled.header(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        flexShrink: 0,
        background: theme!.colors.color4,
        borderBottom: `1px solid ${theme!.colors.color2}`,

        [theme!.mq.small]: {
            height: 64,
            display: 'flex',
            alignItems: 'center',
            margin: '0 24px',
        }
    }
})

const Alignment = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        maxWidth: 1168,
        padding: '8px 0',

        [theme!.mq.small]: {
            height: 64,
            padding: '0 24px',
        }
    }
})

const Info = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        display: 'flex',
        flexDirection: 'column',
        color: theme!.colors.color1,
        fontSize: 10,
        lineHeight: '14px'
    }
})

const LogOutButtonWrapper = styled.div(({ theme }: {
    theme?: Theme
}) => {
    return {
        [theme!.mq.small]: {
            display: 'flex',
            justifyContent: 'flex-end',
            width: 230
        }
    }
})

function Header() {
    const { user, logout } = useAuth()
    const { isMobile } = useMedia()
    const { colors } = useTheme()

    return (
        <Frame>
            <Alignment>
                <LogoLink to={routesList.root}>
                    {isMobile ? <MobileLogo style={{ paddingLeft: 24 }} /> : <DesktopLogo />}
                </LogoLink>
                {!user && (
                    <Info>
                        <Link style={{
                            fontSize: isMobile ? 13 : 17,
                            fontWeight: 600,
                            lineHeight: '150%',
                            textDecoration: 'none',
                            color: colors.color5,
                            margin: 0
                        }} href="tel:88001007269">8 800 100 72 69</Link>
                        <span><FormattedMessage id='app.callFree' /></span>
                    </Info>
                )}
                {user && (
                    <LogOutButtonWrapper>
                        <ButtonMinimal
                            icon={<LogoutIcon style={{ width: 20, height: 20, marginRight: 4 }} />}
                            onClick={() => logout()}
                        >
                            <FormattedMessage id='app.logout' />
                        </ButtonMinimal>
                    </LogOutButtonWrapper>
                )}
            </Alignment>
        </Frame >
    )
}

export {
    Header
}