import { AxiosInstance, AxiosResponse } from 'axios'

import client from './client'

import { GetPersonalInfo_ServerResponse } from './typings/get-personal-info'
import { GetDocument_ServerResponse } from './typings/get-document'
import { PutPersonalInfo_ServerRequest } from './typings/put-personal-info'
import { GetTaxNumber_ServerRequest } from './typings/get-tax-number'
import { GetPersonalBySberId_ServerResponse } from './typings/get-personal-by-sber-id'

type MainGatewayProps = {
    baseUrl: string,
    token: string,
    updateToken: () => any
}

class MainGateway {

    client: AxiosInstance

    constructor({
        baseUrl,
        token,
        updateToken
    }: MainGatewayProps) {
        this.client = client({ baseUrl, token, updateToken })
    }

    getPersonalDataByPhone(phoneNumber: string): Promise<AxiosResponse<GetPersonalInfo_ServerResponse>> {
        return this.client.get(`api/v1/verification/personal-data/phone-number/${phoneNumber}`)
    }

    getDocumentByIdV1(id: string): Promise<AxiosResponse<GetDocument_ServerResponse>> {
        return this.client.get(`api/v2/documents/${id}`)
    }

    getDocumentByIdV2(id: string): Promise<AxiosResponse<File>> {
        return this.client.get(`api/v2/documents/${id}`, { responseType: 'blob' })
    }

    confirmPd(phoneNumber: string): Promise<AxiosResponse> {
        return this.client.post(`api/v1/verification/personal-data/phone-number/${phoneNumber}/confirm`, {})
    }

    rejectPd(phoneNumber: string): Promise<AxiosResponse> {
        return this.client.post(`api/v1/verification/personal-data/phone-number/${phoneNumber}/reject`, {})
    }


    // old methods
    checkPassport(passportSeriesNumber: string): Promise<AxiosResponse<[{ isInvalid: boolean }]>> {
        return this.client.post(`/api/v1/personal/check-passports`, [passportSeriesNumber])
    }

    saveProfile(data: PutPersonalInfo_ServerRequest): Promise<AxiosResponse> {
        return Promise.resolve({
            config: {},
            data: null,
            status: 200,
            statusText: "ok",
            headers: {},
        } as AxiosResponse)


        //return this.client.put(`/api/v2/personal`, data)
    }

    getTaxNumber(data: GetTaxNumber_ServerRequest): Promise<AxiosResponse<{ taxNumber: string }>> {
        return this.client.post(`/api/v1/personal/search/taxnumber`, data)
    }

    async getPersonalInfoBySberId(): Promise<AxiosResponse<GetPersonalBySberId_ServerResponse>> {
        return this.client.get(`/api/v1/personal/sberid`)
    }
}

export {
    MainGateway
}