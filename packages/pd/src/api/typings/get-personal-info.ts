export interface GetPersonalInfo_ServerResponse {
    personalData: PersonalData
    docs?: Document[]
}

type PersonalData = {
    lastName?: string
    name?: string
    patronymic?: string
    birthDate?: string
    phoneNumber?: string
    email?: string
    docType?: string
    docSeries?: string
    docNumber?: string
    docDate?: string
    docOrganization?: string
    taxNumber?: string
}

type Document = {
    name: string
    id: string
    date: string
}