export interface GetDocument_ServerResponse {
	contentType: 'image/png' | 'image/jpeg' | 'application/pdf'
	docBody: File
	docName: string
}