import { GetPersonalInfo_ServerResponse } from './get-personal-info'

export type GetPersonalBySberId_ServerResponse = Omit<GetPersonalInfo_ServerResponse, 'metadata'> 

