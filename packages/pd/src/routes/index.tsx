import { Routes, Route, Navigate } from 'react-router-dom'

import { routesList } from './routes-list'
import { RequireAuth, OnlyAnonymous, RestoreOriginalUrl } from 'routes/require-auth'
import { AppLayout } from 'layouts/app-layout'
import { LoginScreen } from 'screens/login'
import { Screen404 } from 'screens/404'
import { PdVerificationScreen } from 'screens/pd-verification'
import { VerificationListScreen } from 'screens/verification-list'

export function AppRoutes() {

    return (
        <Routes>
            <Route
                path={routesList.login}
                element={(
                    <OnlyAnonymous>
                        <AppLayout>
                            <LoginScreen />
                        </AppLayout>
                    </OnlyAnonymous>
                )}
            />

            <Route
                path={routesList.verificationList}
                element={(
                    <RequireAuth>
                        <AppLayout>
                            <VerificationListScreen />
                        </AppLayout>
                    </RequireAuth>
                )}
            />

            {/* <Route
                path={routesList.verifications}
                element={(
                    <RequireAuth>
                        <AppLayout>
                            <PdVerificationScreen />
                        </AppLayout>
                    </RequireAuth>
                )}
            /> */}

            <Route
                path={routesList.verificationByPhone}
                element={(
                    <RequireAuth>
                        <AppLayout>
                            <PdVerificationScreen />
                        </AppLayout>
                    </RequireAuth>
                )}
            />

            {/* REDIRECT */}
            <Route
                path="/"
                element={(
                    <RestoreOriginalUrl
                        skipUrl={routesList.verificationList}
                    >
                        <Navigate to={routesList.verificationList} />
                    </RestoreOriginalUrl>
                )}
            />

            <Route
                path="*"
                element={(
                    <AppLayout>
                        <Screen404 />
                    </AppLayout>
                )}
            />
        </Routes >
    )
}