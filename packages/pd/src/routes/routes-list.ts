const routesList = {
    root: '/',
    verifications: '/verifications',
    verificationByPhone: '/verifications/:phoneNumber',
    login: '/login',
    verificationList: '/verifications',

    fns: {
        createVerificationByPhoneLink: (phoneNumber: string) => `/verifications/${phoneNumber}`,
    }
}

const redirectUrlKey = 'redirectUrl'

export {
    routesList,
    redirectUrlKey
}