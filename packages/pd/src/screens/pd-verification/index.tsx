import { ErrorBoundary } from 'react-error-boundary'
import { FormattedMessage } from 'react-intl'
import { AiOutlineHome } from 'react-icons/ai'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import { Spinner, Error, Callout, FancyLink } from '@workspace/components'

import { GetPersonalInfo_ServerResponse } from 'api/typings/get-personal-info'
import { routesList } from 'routes/routes-list'
import { PdVerification } from 'modules/pd-verification'
import { useClient } from 'context/auth-context'

export function PdVerificationScreen() {

    const client = useClient()
    const { phoneNumber } = useParams()

    const query = useQuery<HttpSuccessResponse<GetPersonalInfo_ServerResponse>, HttpErrorResponse>(
        [client.getPersonalDataByPhone.toString(), phoneNumber],
        () => client.getPersonalDataByPhone(phoneNumber!)
    )

    return (
        <ErrorBoundary
            FallbackComponent={() => (
                <Error fill />
            )}
        >
            {query.status === 'success' && (
                <PdVerification
                    initialModel={query.data!.data}
                />
            )}

            {query.status === 'loading' && (
                <div style={{ marginTop: 48 }}>
                    <Spinner
                        fill
                        delay={0.5}
                    />
                </div>
            )}

            {query.status === 'error' && query.error.response?.status === 422 && (
                <div style={{
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <Callout style={{ width: 250, marginTop: 24 }} intent='warning' size='small'>
                        {query.error.response?.data.title}
                    </Callout>
                </div>
            )}

            {query.status === 'error' && query.error.response?.status !== 422 && (
                <Error fill style={{ marginTop: 24 }}>
                    {query.error.response?.data.title ? query.error.response.data.title : <FormattedMessage id='app.networkError' />}
                </Error>
            )}

            {query.status === 'error' && (
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    marginTop: 24
                }}>
                    <FancyLink
                        to={routesList.root}
                    >
                        <AiOutlineHome size={16} style={{ marginRight: 4 }} />
                        <FormattedMessage id='app.goHomePage' />
                    </FancyLink>
                </div>
            )}
        </ErrorBoundary>
    )
}