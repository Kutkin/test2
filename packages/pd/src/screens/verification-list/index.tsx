import { ErrorBoundary } from 'react-error-boundary'
import {
    Error,
} from '@workspace/components'

import { VerificationList } from 'modules/verification-list'

export function VerificationListScreen() {
    return (
        <ErrorBoundary
            FallbackComponent={() => (
                <Error fill />
            )}
        >
            <VerificationList />
        </ErrorBoundary>
    )
}